require 'json'
require_relative 'cubic.rb'
require 'pp'

SPLIT = 3

class Solver
end

$faces = []

ARGV.each{|path|
  $faces << Face.new(path)
}

# kiiro url_piece_color = [65535, 54741, 0]
blue = [0, 20817, 47802]
green = [0, 40606, 24672] # 11_
red = [50372, 7710, 14906]
orange = [65535, 22616, 0]
yellow = [65535, 54741, 0] # 0_10
white = [65535, 65535, 65535]

colors = [blue, green, red, orange, yellow, white]

colors =  Piece.all_color

colors.each{|url_piece_color|
  catch(:exit){
    url_pieces = Piece.all.select{|piece|
      piece.detect_color == url_piece_color
    }

    url_pieces.each{|piece|
      piece.detect_type!
    }

    corner_pieces = url_pieces.select{|piece| piece.type == :corner}
    edge_pieces = url_pieces.select{|piece| piece.type == :edge}
    center_piece = url_pieces.select{|piece| piece.type == :center}.first

    #pp corner_pieces
    #pp edge_pieces
    #pp center_piece

    corner_condition_master = [0, 1, 2, 3]
    edge_condition_master = [0, 1, 2, 3]
    center_rotates = [0, 90, 180, 270]

    i = 0
    corner_condition_master.permutation(4).each{|corner_condition|
      edge_condition_master.permutation(4).each{|edge_condition|
        center_rotates.each{|center_rotate|
          image_list = ImageList.new
          image_list << corner_pieces[corner_condition[0]].normalized_image(0)
          image_list << edge_pieces[edge_condition[0]].normalized_image(0)
          image_list << corner_pieces[corner_condition[1]].normalized_image(90)
          image_list << edge_pieces[edge_condition[1]].normalized_image(270)
          image_list << center_piece.normalized_image(center_rotate)
          image_list << edge_pieces[edge_condition[2]].normalized_image(90)
          image_list << corner_pieces[corner_condition[2]].normalized_image(270)
          image_list << edge_pieces[edge_condition[3]].normalized_image(180)
          image_list << corner_pieces[corner_condition[3]].normalized_image(180)

          size = image_list.first.rows

          fname = "to_decode_#{Time.now.strftime('%H%M%S')}_#{i += 1}.png"
          image_list.montage{
            self.tile = "#{SPLIT}x#{SPLIT}"
            self.geometry = "#{size}x#{size}+0+0"
          }.write(fname)

          result = QR.decode(fname)
          if result
            puts result
            if result.include?('http')
              puts `ruby dl.rb #{result}`
              exit
            end
            throw :exit
          end
        }
      }
    }
  }
}
