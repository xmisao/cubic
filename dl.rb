require 'open-uri'

class Downloader
  def self.download(url)
    puts url

    html = open(url){|f| f.read}

    downloaded_files = []

    html.scan(/new loader.load\(.*"(.*)".*\)/){|m|
      image_file = m[0][1..-1]

      image_url = BASE_URL + image_file

      local_image_path = image_file
      open(local_image_path, 'w'){|f|
        puts image_url
        f.print open(image_url){|f2| f2.read}
      }

      downloaded_files << local_image_path
    }

    downloaded_files
  end
end

url = ARGV[0]
BASE_URL = 'http://qubicrube.pwn.seccon.jp:33654/'

Downloader.download(url)
