require 'sinatra'
require 'json'
require_relative 'cubic.rb'

SPLIT = 3

$faces = []

ARGV.each{|path|
  $faces << Face.new(path)
}

get '/' do
  @faces = $faces
  erb :index
end

get "/pieces/:name" do
  expires 500, :public, :must_revalidate
  Piece.search(params[:name]).blob
end

post "/decode", provides: :json do
  piece_info = JSON.parse request.body.read

  image_list = ImageList.new
  piece_info.each{|pi|
    image = Piece.search(pi['name']).image
    image = image.rotate(pi['rotate']) unless pi['rotate'] == 0
    image_list << image
  }

  size = image_list.first.rows

  fname = "to_decode_#{Time.now.strftime('%H%M%S')}.png"
  image_list.montage{
    self.tile = "#{SPLIT}x#{SPLIT}"
    self.geometry = "#{size}x#{size}+0+0"
  }.write(fname)

  result = QR.decode(fname)

  {decoded: result}.to_json
end
