require 'rmagick'
include Magick

class Face
  def pieces
    @pieces
  end

  def initialize(path)
    @pieces = []
    load_image(path)
  end

  def load_image(path)
    img = ImageList.new(path)

    square = 246 / SPLIT
    square_center = (square / 2).to_i

    SPLIT.times{|cell_y|
      SPLIT.times{|cell_x|
        x_offset = square * cell_x
        y_offset = square * cell_y

        piece_image = img.crop(x_offset, y_offset, square, square)

        @pieces << Piece.new(piece_image)
      }
    }
  end
end

class Piece
  @@counter = 0
  @@pieces = {}

  def name
    @name
  end

  def initialize(image, color = nil)
    @image, @color = image, color
    @name = (@@counter += 1).to_s
    @@pieces[@name] = self
    @color = nil
  end

  def blob
    @image.to_blob
  end

  def image
    @image
  end

  def self.search(name)
    p @@pieces[name]
  end

  def detect_color
    return @color if @color

    colors = []

    cx = (@image.columns / 2).to_i
    cy = (@image.rows / 2).to_i

    (-8..8).each{|x|
      (-8..8).each{|y|
        color = @image.pixel_color(cx + x, cy + y)
        colors << [color.red, color.green, color.blue]
      }
    }

    @color ||= colors.uniq.select{|v| v != [0, 0, 0]}.first
  end

  def self.all
    @@pieces.values
  end

  def self.all_color
    @@pieces.values.map(&:detect_color).uniq.sort
  end

  def detect_type!
    rotates = [0, 90, 180, 270]
    candidates = []

    rotates.each do |rotate|
      rotated_image = rotate == 0 ? @image : @image.rotate(rotate)

      w = 8

      top_colors = []
      (0..w).each{|y|
        (0..rotated_image.columns).each{|x|
          color = rotated_image.pixel_color(x, y)
          top_colors << [color.red, color.green, color.blue]
        }
      }

      left_colors = []
      (0..rotated_image.rows).each{|y|
        (0..w).each{|x|
          color = rotated_image.pixel_color(x, y)
          left_colors << [color.red, color.green, color.blue]
        }
      }

      if top_colors.uniq.count == 1 && left_colors.uniq.count == 1
        type = :corner
        normalized_rotate = rotate

        candidates << [type, normalized_rotate]
      elsif top_colors.uniq.count == 1 
        type = :edge
        normalized_rotate = rotate

        candidates << [type, normalized_rotate]
      else
        type = :center
        normalized_rotate = rotate

        candidates << [type, normalized_rotate]
      end
    end

    if result = candidates.select{|cand| cand[0] == :corner}.first
      @type = result[0]
      @normalized_rotate = result[1]
    elsif result = candidates.select{|cand| cand[0] == :edge}.first
      @type = result[0]
      @normalized_rotate = result[1]
    elsif result = candidates.select{|cand| cand[0] == :center}.first
      @type = result[0]
      @normalized_rotate = 0 # NOTE default rotation
    else
      raise candidates.inspect
    end
  end

  def type
    @type
  end

  def normalized_rotate
    @normalized_rotate
  end

  def normalized_image(rotate)
    if (r = rotate + @normalized_rotate) % 360 == 0
      @image
    else
      @image.rotate(r)
    end
  end
end

class QR
  def self.decode(fname)
    result = `zbarimg #{fname}`
    result.match(/^QR-Code:(.*?)$/).to_a[1]
  end
end
